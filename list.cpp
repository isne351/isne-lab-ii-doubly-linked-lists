#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for(Node *p; !isEmpty(); ) {
		p=head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head,0);
	if(tail==0)
	{
		tail = head;
	}
	else
	{	
		head->next->prev = head;
	}
}
void List::pushToTail(char el)
{
	Node *tmp;
	
	tmp = new Node(el,0,tail);
	if (head == 0){
		tail = head;
	}
	else
	{
		tail->next = tmp;

		tail = tmp;
	}
}
char List::popHead()
{
	char el = head->data;
	Node *tmp = head;
	if(head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
	}
	delete tmp;
	return el;
}
char List::popTail()
{
	Node *tmp = tail->prev;

	delete tail;

	tail = tmp;
	tail->next = NULL;

	return NULL;
}
bool List::search(char el)
{

	Node *tmp = head;
	while (tmp != NULL) {
		if (el == tmp-> data) return true;
		tmp = tmp -> next;
	}

	// TO DO! (Function to return True or False depending if a character is in the list.
	return NULL;
}
void List::print()
{
	if(head  == tail)
	{
		cout << head->data;
	}
	else
	{
		Node *tmp = head;
		while(tmp!=tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}