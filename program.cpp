#include <iostream>
#include "list.h"
using namespace std;

void main()
{

	List mylist;

	cout << "insert 1 2 3 to head : ";
	mylist.pushToHead('1');
	mylist.pushToHead('2');
	mylist.pushToHead('3');

	mylist.print();

	cout << endl;

	mylist.pushToTail('4');

	cout << "insert 4 to tail : ";

	mylist.print();

	cout << endl;

	cout << "Seach 4 ";

	if (mylist.search('4')) {
		cout << " found" << endl;
	}
	else {
		cout << " not found" << endl;
	}

	mylist.popTail();

	cout << "pop 4 from tail : ";

	mylist.print();

	cout << endl;

	cout << "Seach 4 ";

	if (mylist.search('4')) {
		cout << " found" << endl;
	}
	else {
		cout << " not found" << endl;
	}

	cout << "i can't complie code when use template (Error LNK1561)" << endl;

	system("pause");
}